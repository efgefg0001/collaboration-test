# Purpose
This is a repository for testing collaboration using git.

This is numbered list:

1. num 1
2. num 2
3. num 3
4. num 4
5. num 5

This is list of useful resources:

* [google.com][google]
* [yandex.ru][yandex]
* [bitbucket.org][bitbucket]
* [github.com][github]

This is a list of fruit:

1. Apples
2. Bananas
3. Grapes
4. Pineapples
5. Strawberry
6. Blueberry

Images:

![Grape][grape]

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

---

Header #1 | Header #2 
----------|----------
1         | 2         
3         | 4         

---

Right | Left | Center
-----:|:-----|:-----:
 r    | l    | c
 R    | L    | C

[google]: https://www.google.com
[yandex]: https://www.yandex.ru
[bitbucket]: https://www.bitbucket.org
[github]: https://www.github.com


[grape]: img/grape.jpg
